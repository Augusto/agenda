public class Pessoa{
	private String nome;
	private String telefone;
	private int idade;
	private String cpf;

	public Pessoa(){
		
		
	}
		
	public Pessoa( String nome, String cpf){
		this.setNome(nome);
		this.setCpf(cpf);
	}

	public Pessoa( String telefone){
		this.setCpf(telefone);
	}

	public void setNome( String nome ){
		this.nome = nome;
	}

	public String getNome( ){
		return this.nome;
	}
	
	public void setTelefone( String telefone ){
		this.telefone = telefone;
	}

	public String getTelefone(){
		return this.telefone;
	}	

	public void setIdade( int idade){
		this.idade = idade;
	}

	public int getIdade(){
		return this.idade;
	}

	public void setCpf( String cpf){
		this.cpf = cpf;
	}

	public String getCpf(){
		return this.cpf;
	}


}
