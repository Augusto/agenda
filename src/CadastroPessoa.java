import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CadastroPessoa{
	public static void main(String[] args) throws IOException{
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);

		ControllerPessoa umControle = new ControllerPessoa();
		
		int numeroMenu = 0;
		while(numeroMenu != 5){
			System.out.println("Menu agenda");
			System.out.println("1. Adicionar contato");
			System.out.println("2. Excluir contato");
			System.out.println("3. Pesquisar contato");
			System.out.println("4. Listar todos os contatos");
			
			System.out.println("Digite o numero da ação que deseja fazer: ");
			numeroMenu = Integer.parseInt(leitorEntrada.readLine());
			while(numeroMenu != 1 && numeroMenu != 2 && numeroMenu != 3 && numeroMenu != 4){
				System.out.println("Digite um número válido");
				numeroMenu = Integer.parseInt(leitorEntrada.readLine());
			}
			
			switch(numeroMenu){
			  case 1:	
				System.out.println("");
				System.out.println("");
				Pessoa umaPessoa = new Pessoa();
				System.out.println("Digite o nome: ");
				umaPessoa.setNome(leitorEntrada.readLine());
				System.out.println("Digite o telefone: ");
				umaPessoa.setTelefone(leitorEntrada.readLine());
				System.out.println("");
				System.out.println(umControle.adicionar(umaPessoa)); 
				System.out.println("Pressione qualquer tecla para voltar ao menu");
				leitorEntrada.readLine();
				break;
			  case 2:
				System.out.println("");
				System.out.println("");
				System.out.println("Digite o nome: ");
				Pessoa objetoPessoa = umControle.localizar(leitorEntrada.readLine());
				try {
					System.out.println(umControle.excluir(objetoPessoa));
				} catch (Exception e) {
					System.out.println("Pessoa não encontrada");
				}
				System.out.println("Pressione qualquer tecla para voltar ao menu");
				leitorEntrada.readLine();
				break;
			 case 3:
				System.out.println("");
				System.out.println("");
				System.out.println("Digite o nome: ");
				Pessoa objetoPessoa = umControle.localizar(leitorEntrada.readLine());
				if(objetoPessoa != null){
					System.out.println("");
					System.out.println("");
					System.out.println("Nome: " + objetoPessoa.getNome());
					System.out.println("Telefone: " + objetoPessoa.getTelefone());
					System.out.println("Idade: " + objetoPessoa.getIdade());
					System.out.println("Cpf: " + objetoPessoa.getCpf());
				}
				else{
					System.out.println("Pessoa não encontrada");
				}
				System.out.println("Pressione qualquer tecla para voltar ao menu");
				leitorEntrada.readLine();
				break;
			case 4:
				System.out.println("");
				System.out.println("");
				ArrayList<Pessoa> listaPessoas = umControle.getListaPessoas();
				
				for(int i = 0; i < listaPessoas.size(); i++){
					System.out.println(i + ". " + listaPessoas.get(i).getNome());
				}
				System.out.println("Pressione qualquer tecla para voltar ao menu");
				leitorEntrada.readLine();
				break;
			}
			
		}
		
		
		
	}
}
