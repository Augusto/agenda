import java.util.ArrayList;

public class ControllerPessoa{
	private ArrayList<Pessoa> listaPessoas;
	private Pessoa novaPessoa;

	public ControllerPessoa(){
		this.listaPessoas = new ArrayList<Pessoa>();
	}
	
	public void adicionar( String nome, String telefone, int idade, String cpf){
		this.novaPessoa = new Pessoa( nome, cpf);
		this.novaPessoa.setIdade(idade);
		this.novaPessoa.setTelefone(telefone);
		this.listaPessoas.add(novaPessoa);
	}
	
	public String adicionar( Pessoa novaPessoa){
		this.listaPessoas.add(novaPessoa);
		return "Pessoa adicionada com sucesso";
	}

	public String excluir( Pessoa umaPessoa ){
		this.listaPessoas.remove(umaPessoa);
		return umaPessoa.getNome() + " removido com sucesso";
	}

	public Pessoa localizar( String nome){
		for(Pessoa umaPessoa: listaPessoas){
			if( umaPessoa.getNome().equalsIgnoreCase(nome)) return umaPessoa;
		}
		return null;
	}

	public ArrayList<Pessoa> getListaPessoas(){
		return this.listaPessoas;
	}

}
